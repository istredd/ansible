#/bin/bash

if [ "$2" = "disabled" ]; then
  usermod $1 -e 1 && echo "$1 disabled"
else
  usermod $1 -e -1 && echo "$1 enabled"
fi
