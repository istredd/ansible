---
  - name: Prevent 'all' tag from running
    fail: msg="This playbook must be run with either 'install_modsec', 'install_crs', or 'uninstall'"
    tags: all

  - name: Change state value
    set_fact: modstate="absent"
    tags: uninstall

  - name: Install modsecurity
    apt: name=libapache2-modsecurity state="{{ modstate }}"
    tags: ['install_modsec', 'uninstall']

  - name: Create config directory
    file: path=/etc/modsecurity state=directory
    tags: install_modsec

  - name: Modsecurity unicode file copy
    copy: src=unicode.mapping dest={{ ModSecurityConfPath }}/unicode.mapping
    tags: install_modsec

  - name: Modsecurity configuration file creation
    template: src=modsecurity.conf.j2 dest={{ ModSecurityConfPath }}/modsecurity.conf owner=root group=root mode=0644
    notify: restart apache
    tags: install_modsec

  - name: Ensure modsecurity module is (un)loaded
    apache2_module: name=security2 state="{{ modstate }}"
    tags: ['install_modsec', 'uninstall']

  - name: Install CRS rules
    git: repo=git@gitlab.:sysadmin/owasp-modsecurity-crs.git dest={{ ModSecurityConfPath }}/owasp ssh_opts="-o StrictHostKeyChecking=no"
    tags: install_crs

  - name: Find all available rules
    find: paths={{ ModSecurityConfPath }}/owasp/base_rules patterns=* recurse=yes
    register: findresult
    tags: install_crs

  - name: Create symlinks to rules
    file: src={{ item.path }} dest={{ ModSecurityConfPath }}/owasp/activated_rules/{{ item.path | basename }} state=link
    with_items: findresult.files
    tags: install_crs

  - name: Create link to CRS config file
    file: src={{ ModSecurityConfPath }}/owasp/modsecurity_crs_10_setup.conf.example dest={{ ModSecurityConfPath }}/owasp/modsecurity_crs_10_setup.conf state=link
    tags: install_crs

  - name: Update Apache config file
    blockinfile:
      dest: /etc/apache2/apache2.conf
      state: "{{ modstate }}"
      block: |
        <IfModule security2_module>
            Include {{ ModSecurityConfPath }}/owasp/modsecurity_crs_10_setup.conf
            Include {{ ModSecurityConfPath }}/owasp/activated_rules/*.conf
        </IfModule>
    notify: restart apache
    tags: ['install_crs', 'uninstall']

  - name: Remove CRS rules and config files
    file: path=/etc/modsecurity/ state=absent
    tags: uninstall

  - name: Restart Apache service
    service: name=apache2 state=restarted
    tags: ['install_crs', 'install_modsec', 'uninstall']
