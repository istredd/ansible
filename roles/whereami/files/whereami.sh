#!/bin/bash

###############################
# Bart Labno * May 2016 *     #
# Script to check location,   #
# date, time and uid          #
###############################

echo
echo " Hostname: $(tput bold && tput setaf 2)$(hostname -f)$(tput sgr0)"
echo " IPv4:     $(tput bold && tput setaf 2)$(ifconfig | grep -i Bcast | cut -d: -f 2 | awk '{print $1}')$(tput sgr0)"
echo " User:     $(tput bold && tput setaf 2)$(id -un) - $(id -ur)$(tput sgr0)"
echo " Location: $(tput bold && tput setaf 2)$(pwd)$(tput sgr0)"
echo " Date:     $(tput bold && tput setaf 2)$(date)$(tput sgr0)"
echo
