# Sysadmin repository

This is a generic repository to manage some servers.

## Ansible

Ansible is a tool written in python to automate deployemnt process on group of servers which contains specific ssh public key. There is an ansible tool which needs to be installed on machine which will be doing deployment process also private keys are required to access any of endpoint clients. Public keys can be published into nodes by the person who already has an admin privileges to deploy or by ansible user which also has admin privileges.

### Requirements

#### Ansible server

##### Software:
- ansible 2.0.2 (or never)
- git
- text editor (i.e. vim, atom)

##### Additional requirements
- gitlab access to sysadmin group
- vault key (to access sensitive data)
- user with ssh keypair (public key added to ansible repository) with sudo privs


To be more specific Ansible does not require to install server on any of machine. Ansible by itself requires ansible application and private key with privileges to use sudo on node to execute any role or task. There is a requirement to install minimum version of ansible 2.0.x on client involved in deployment process as some of roles were not introduced before that version. Ansible in simple way stores all deployment roles and tasks in specific structure which is used by itself to make deployment proccess succesfull. Sysadmin ansible playbooks are stored in one gitlab private repository so also someone needs to add new user to gitlab sysadmin group with access into ansible role.

#### Ansible node

As ansible is agentless application and uses only ssh keys to deploy anything. That makes there is a minimum requirement of one public key provided with sudo privileges to deploy something with ansible. This is enough to trigger role which will deploy another users privileges, firewall rules, server configuration and other playbooks if required (depending on nodes role).

### Structure


        ├── ansible.cfg               <= additional ansible configuration
        ├── generic.yaml              <= playbook name
        ├── host_vars                 <= specific variables assigned to host
        │   └── ansible
        ├── inventory                 <= all hosts defined to deploy with ansible. These can be assigned to specific groups or variables
        │   └── hosts
        ├── ntp.yaml                  <= playbook name
        ├── plugins                   <= additional plugins required to play
        │   └── lookup_plugins
        │       └── hashi_vault.py
        └── roles                     <= roles used by playbooks in very strict structure
            ├── ansible               <= ansible role to install or upgrade ansible in newest version
            │   └── tasks
            │       └── main.yml
            ├── firewall              <= ansible role to manage firewall
            │   ├── defaults          <= default variables (if not defined) used in tasks
            │   │   └── main.yml
            │   ├── handlers          <= handlers file
            │   │   └── main.yml
            │   ├── meta              <= role dependencies
            │   │   └── main.yml
            │   ├── tasks             <= tasks to be run when role is triggered; tasks can contain smaller files included in role
            │   │   └── main.yml
            │   ├── templates         <= configuration file templates based on jinja2 language
            │   │   ├── firewall.bash.j2
            │   │   └── firewall.j2
            │   └── tests             <= tests to be run
            │       ├── inventory
            │       └── test.yml
            ├── gitlab                <= installs and upgrades gitlab
            ├── install               <= installs and upgrades packages
            ├── java                  <= installs java
            ├── jenkins               <= installs jenkins (java and vault required)
            ├── jenkins-backup        <= creates jenkins backup on specific backup server
            ├── jenkins-restore       <= restores jenkins from specific backup server
            ├── mercurial             <= installs mercurial on node
            ├── ntp                   <= installs and configures ntp synchronization
            ├── postfix               <= installs and configures postfix
            ├── user-management       <= adds/removes users, assigns to groups, manages sudo, deploy public keys, sets passwords
            ├── vault                 <= installs vault with encrypted sudo password for ansible user for jenkins
            └── whereami              <= installs small script with useful data



### Syntax

        ansible-playbook [-i hosts_file] playbook.yml [-e 'extra_variable=value'] [-u username --private_key]

ansible-playbook triggers specific playbook with additional options if required. For most of playbooks to trigger playbook these options will be used:
- -i - informs ansible about location of hosts file instead of /etc/ansible/hosts. This is already specified in ansible.cfg so option not required to use
- -e - extra variables passed with ansible. Useful in some situations - for example to update specific user password or upgrade packahges or install additional software
- -K - requests become password [sudo for most of playbooks]
- -u - runs ansible as other user than currently logged in. Useful to trigger playbooks as ansible user
- --private-key - path to private key. Useful when playing as different user
- --ask-vault-pass - option required to decrypt file with sensitive data (look ansible-vault to learn more about files encryption). Useful to change users password

        ansible-vault [option] path/to/encrypted/file

ansible-vault works locally on file which requires to be encrypted. ansible-vault decrypts file and open editor (default is vim) to change any sensitive data. On file creation vault is setting password to decrypt file. All files are encrypted with AES256 and there is no possibility to view or edit them without password provided. --ask-vault-pass is required when playbook runs to read data from encrypted file.
- create - create new file with password specified
- edit - edit file with editor
- encrypt - encrypt existing file with password
- decrypt - decrypt existing file
- rekey - change password

### Playbooks

There is a couple of currently available. Some of additional option can be provided to trigger them.

#### generic

This is basic role which is triggered on every git push to master. There is a couple of roles involved in that playbookt (ie ntp, postfix, whereami, install) but no specific options required to provide. This job is best to keep all nodes update, with all required users, applications and basic configuration. This playbook is also available on jenkins and is triggered on every git push

#### gitlab

This role deploys gitlab installation into specific node. There are no specific options to provide.

#### jenkins

There are three separate roles which are working together with jenkins server.

##### deploy

Basic playbook which installs jenkins and java on specific node. This playbook is responsible to deploy jenkins or update it into newest version but with no any data. There are no specific options to provide

##### backup

This role is creating backups of jenkins instances into specified storage (read from variable). This playbook doesn't require additional options but is triggered daily by jenkins to crate a new backup

##### restore

This playbook is deploying jenkins database, and config files from latest available backup to jenkins node. At the moment it uses latest backup file from storage and deploys inst jenkins. Also that role deploys local vault on jenkins which keeps ansible key and sudo password. To do a quick restore of jenkins when it is broke simply trigger jenkins-deploy and jenkins-restore playbooks to get jenkins working from last day.

#### users

This is complex playbook to change access level for users on nodes. It can be triggered with couple of additional options but basicaly manages to check if users specified exists on nodes.

usage:

        ansible-playbook users.yaml [options]

options:

        -e 'deployuser=login' --ask-vault-pass

Option deployuser manages on all nodes to set specific password stored in password file. Password file must be stored in vars/login.yaml where login is existing login name. That option requires to encrypt login file as it stores password used for sudo.

That playbook deploys specific user login to nodes. It requires to define users with access level in vars/main.yml but also requires to define public_key file in files folder (named login.pub) and password file in vars/login.yaml if sudo granted for user.

##### vars/main.yaml
contains all users which have to be added or removed from nodes. Structure of file must be kept as it is to avoid failing of playbook. When there is no users in specific group then all block must be commented.

##### password file example:

        ---
          password: safepassword


## Jenkins

### Overview

### Usage

### Jobs

### Backup

## GitLab

## Storage
